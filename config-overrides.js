const { override, fixBabelImports, addLessLoader } = require('customize-cra');

module.exports = override(
  // fixBabelImports("antd", {
  //   libraryDirectory: "es",  // 这段不需要了 lib 或 es 的 v4的侧边Slider的Menu 在 收缩时有问题 不如直接在主文件引样式
  //   libraryName:'antd',
  //   style: true,
  // }),
    addLessLoader({
      lessOptions: {
        // If you are using less-loader@5 please spread the lessOptions to options directly
        javascriptEnabled: true,
        modifyVars: { "@primary-color": "#1DA57A" },
      },
    })
);
