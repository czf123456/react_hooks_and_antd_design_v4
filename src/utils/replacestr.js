// 将图片化成文本消息
export default function(str) {
    if(typeof str==='string') {
        str = str.replace(/<img(.*?)\/>/g,'[: 图片 :]');
        return str
    };
    return str
}