import $ from 'jquery'
// 气泡弹窗函数
export const drawBubble = function (ctx, x, y, w, h, infor) {
  //左上角点(x,y) 整体宽高(w,h)
  const gradient = ctx.createLinearGradient(0, 0, w, 0);
  gradient.addColorStop("0", "#fff14d");
  gradient.addColorStop("0.5", "#ff7331");
  gradient.addColorStop("1.0", "#9453f8");
  ctx.beginPath();
  ctx.moveTo(x, y + 0.35 * h);
  ctx.lineWidth = 2;
  ctx.shadowBlur = 20;
  ctx.shadowOffsetX = -2;
  ctx.shadowOffsetY = 2;
  ctx.shadowColor = "rgba(0,0,0,.3)";
  ctx.quadraticCurveTo(x + 0.04 * w, y + 0.01 * h, x + 0.5 * w, y);
  ctx.quadraticCurveTo(x + 0.96 * w, y + 0.01 * h, x + 0.98 * w, y + 0.4 * h);
  ctx.quadraticCurveTo(x + w, y + 0.9 * h, x + 0.2 * w, y + 0.72 * h);
  ctx.quadraticCurveTo(x + 0.1 * w, y + 0.8 * h, x + 0.01 * w, y + h);
  ctx.quadraticCurveTo(x, y + 0.9 * h, x + 0.1 * w, y + 0.72 * h);
  ctx.quadraticCurveTo(x, y + 0.6 * h, x, y + 0.35 * h);
  ctx.fillStyle = gradient;
  ctx.strokeStyle = gradient;
  ctx.textBaseline = "middle";
  ctx.font = "bold 16px sans-serif";
  ctx.fillText(infor, 25, 30);
  ctx.stroke();
};

// 随机颜色

export const randomColor = function () {
  var r = Math.floor(Math.random() * 125);
  var g = Math.floor(Math.random() * 125);
  var b = Math.floor(Math.random() * 125);
  return "rgb(" + r + "," + g + "," + b + ")";
};
// 验证码生成函数
export const drawVerfiCode = function (ctx, width, height, numarr) {
  ctx.clearRect(0, 0, 80, 40);
  var sCode =
    "a,b,c,d,e,f,g,h,i,j,k,m,n,p,q,r,s,t,u,v,w,x,y,z,A,B,C,E,F,G,H,J,K,L,M,N,P,Q,R,S,T,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0";
  var aCode = sCode.split(",");
  var aLength = aCode.length; //获取到数组的长度

  for (var i = 0; i < 4; i++) {
    //这里的for循环可以控制验证码位数（如果想显示6位数，4改成6即可）
    var j = Math.floor(Math.random() * aLength); //获取到随机的索引值
    // var deg = Math.random() * 30 * Math.PI / 180;//产生0~30之间的随机弧度
    var deg = Math.random() - 0.5; //产生一个随机弧度
    var txt = aCode[j]; //得到随机的一个内容
    numarr[i] = txt.toLowerCase();
    var x = 0 + i * 20; //文字在canvas上的x坐标
    var y = 25 + Math.random() * 8; //文字在canvas上的y坐标
    ctx.font = "bold 25px Arial";

    ctx.translate(x, y);
    ctx.rotate(deg);

    ctx.fillStyle = 'white';
    ctx.fillText(txt, 0, 0);

    ctx.rotate(-deg);
    ctx.translate(-x, -y);
  }
  for (i = 0; i <= 5; i++) {
    //验证码上显示线条
    ctx.strokeStyle = randomColor();
    ctx.beginPath();
    ctx.moveTo(Math.random() * width, Math.random() * height);
    ctx.lineTo(Math.random() * width, Math.random() * height);
    ctx.stroke();
  }
  for (i = 0; i <= 30; i++) {
    //验证码上显示小点
    ctx.strokeStyle = randomColor();
    ctx.beginPath();
    x = Math.random() * width;
    y = Math.random() * height;
    ctx.moveTo(x, y);
    ctx.lineTo(x + 1, y + 1);
    ctx.stroke();
  }
  return numarr;
};

// 拼图核心画法
export const drawSlider = function (ctx, x, y, operation, l, r) {
  let PI = Math.PI;
  ctx.beginPath();
  ctx.shadowColor = "black";
  if (operation === "clip") {
    ctx.shadowOffsetX = 2;
    ctx.shadowOffsetY = 2;
    ctx.shadowBlur = 3;
  } else {
    ctx.shadowOffsetX = -1;
    ctx.shadowOffsetY = -1;
    ctx.shadowBlur = 1;
  }
  ctx.moveTo(x, y);
  ctx.arc(x + l / 2, y - r + 2, r, 0.72 * PI, 2.26 * PI);
  ctx.lineTo(x + l, y);
  ctx.arc(x + l + r - 2, y + l / 2, r, 1.21 * PI, 2.78 * PI);
  ctx.lineTo(x + l, y + l);
  ctx.lineTo(x, y + l);
  ctx.arc(x + r - 2, y + l / 2, r + 0.4, 2.76 * PI, 1.24 * PI, true);
  ctx.lineTo(x, y);
  if (operation === "clip") {
    ctx.lineWidth = 2;
    ctx.strokeStyle = "rgba(255, 255,255,0.6)";
  } else {
    ctx.lineWidth = 2;
    ctx.fillStyle = "rgba(255,255,255,0.3)";
    ctx.strokeStyle = "rgba(255, 255, 255,0.8)";
  }
  ctx.closePath();
  ctx.stroke();
  ctx.fill();
  ctx[operation]();
  if (operation == 'clip') {
    ctx.globalCompositeOperation = "source-in";
  }else {
    
    ctx.globalCompositeOperation = 'destination-over'
  }


   // 上面的绘图根据这里的属性选择新图像的类型
};

// 随机拼图位置
export const getRandomSliderPosition = function (start, end) {
  return Math.floor(Math.random() * (end - start) + start);
};

// 图片的加载
export const drawPic = (ctx2, w, h, imgPath) => {
  // 生成图片的构造函数
  let img = new Image() ;
  img.onload = function() {
   ctx2.drawImage(img, 0, 0, w, h);
  }
  img.src = imgPath ;

};
