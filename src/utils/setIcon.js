export const setIcon = (focusItem, errorWord, num) => {
    if ((focusItem === 0 && errorWord === "") || errorWord === undefined) {
      return "";
    } else {
      if (errorWord !== "") {
        return "error";
      } else {
        let value = focusItem === num ? "success" : "";
        return value;
      }
    }
  }; 