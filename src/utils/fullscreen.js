export function requestFullScreen() {
    // 判断各种浏览器，找到正确的方法
    const requestMethod = document.documentElement.requestFullScreen || // W3C
    document.documentElement.webkitRequestFullScreen || // FireFox
    document.documentElement.mozRequestFullScreen || // Chrome等
    document.documentElement.msRequestFullScreen // IE11
    if (requestMethod) {
      requestMethod.call(document.documentElement)
    } else if (typeof window.ActiveXObject !== 'undefined') { // for Internet Explorer
      var wscript = new window.ActiveXObject('WScript.Shell')
      if (wscript !== null) {
        wscript.SendKeys('{F11}')
      }
    }
  }

  export function exitFull() {
    // 判断各种浏览器，找到正确的方法
    var exitMethod = document.exitFullscreen || // W3C
        document.webkitExitFullscreen || // Chrome等
        document.mozCancelFullScreen || // FireFox
        document.msExitFullscreen // IE11
    if (exitMethod) {
      exitMethod.call(document)
    } else if (typeof window.ActiveXObject !== 'undefined') { // for Internet Explorer
      var wscript = new window.ActiveXObject('WScript.Shell')
      if (wscript !== null) {
        wscript.SendKeys('{F11}')
      }
    }
  }
  
  export function isFullscreenForNoScroll(){
    var explorer = window.navigator.userAgent.toLowerCase();
    if(explorer.indexOf('chrome')>0){//webkit
        if (document.body.scrollHeight === window.screen.height && document.body.scrollWidth === window.screen.width) {
            alert("全屏");
        } else {
            alert("不全屏");
        }
    }else{//IE 9+  fireFox
        if (window.outerHeight === window.screen.height && window.outerWidth === window.screen.width) {
            alert("全屏");
        } else {
            alert("不全屏");
        }
    }
}
