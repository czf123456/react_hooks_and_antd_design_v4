export default function loadingImages(arr) {
  // 多图片预加载
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (Array.isArray(arr)) {
        for (var i = 0; i <= arr.length - 1; i++) {
          var image = new Image();
          window.onload = function () {
            
          };
          image.src = arr[i];
        }
        resolve(arr);
      }
    }, 10);
  });
}
