// 防抖函数
export function debounce(func, wait = 500) {
  let timeout;
  return function (event) {
    clearTimeout(timeout);
    event.persist && event.persist(); // 保留对事件的引用
    timeout = setTimeout(() => {
      func(event);
    }, wait);
  };
}

// 节流函数
export function throttle(func, interval = 100) {
  let timeout;
  let starttime = new Date();
  return function (event) {
    event.persist && event.persist();
    clearTimeout(timeout);
    let endtime = new Date();
    if (endtime - starttime <= interval) { // 如果小于等于时间间隔 那就等到时间间隔到再执行 但是被刷新不会执行
     timeout = setTimeout(() => {
        func(event);
      }, interval);
    }else {
        starttime = endtime ; // 每达到0.1s的间隔
        func(event); // 此处才是代码真正的执行
    }
  };
}
