import React from "react";
import "./about.scss";
import { Card } from "antd";
import Word from "../../components/Word/Index";
function Index() {
  return (
    <div className="about">
      <div style={{ position: "absolute" }}>
        <Word>
          <div className="markdown" style={{ color: "white" }}>
            <h2 style={{color:'yellow'}}>嗨</h2>
            <br/>
            <h2 style={{color:'yellow'}}>程序员的生活很简单,</h2>
            <br />
            <h2 style={{color:'yellow'}}> 就是天天敲代码,</h2>
            <br/>
            <h2 style={{color:'yellow'}}>很苦的差事,</h2>
            <br />
            <h2 style={{color:'yellow'}}>但必须苦中做乐。</h2>
            <br/>
            <h2>希望每位程序员的挑灯夜战,</h2>
            <br />
            <h2> 最后都能有累累的硕果。</h2>
            <br />
            <h2> 我只是一个平凡的前端小鸟,</h2>
            <br />
            <h2>飞了一年了。</h2>
            <br />
            <h2>从开始的不屑,</h2>
            <br />
            <h2>到现在的慎重,</h2>
            <br />
            <h2>看到了前端的本质,</h2>
            <br />
            <h2>你若简单他便简单,</h2>
            <br />
            <h2>你也不会接触复杂的代码。</h2>
            <br />
            <h2>梦想也会难以实现。</h2>
            <br />
            <h2>真正的踏实做了,</h2>
            <br />
            <h2>才发现,</h2>
            <br />
            <h2>即使前端也永无止境。</h2>
            <br />
            <h2>努力地克服困难吧。</h2>
            <br/>
            <h2>向着前端高深的领域迈进.....</h2>
          </div>
        </Word>
      </div>

      <div className="main" style={{ backgroundColor: "black" }}>
        <div className="human">
          <div className="human__hair-b"></div>
          <div className="human__head">
            <div className="human__face">
              <div className="human__hair-c">
                <div className="human__hair"></div>
              </div>
              <div className="human__nose"></div>
              <div className="human__eye-l"></div>
              <div className="human__eye-r"></div>
              <div className="human__cheeks-l"></div>
              <div className="human__cheeks-r"></div>
              <div className="human__mouth"></div>
              <div className="human__eyebrow-l"></div>
              <div className="human__eyebrow-r"></div>
            </div>
          </div>
          <div className="human__ears">
            <div className="human__ear-l"></div>
            <div className="human__ear-r"></div>
          </div>
          <div className="human__arm-l"></div>
          <div className="human__arm-r"></div>
          <div className="human__body-c">
            <div className="human__body"></div>
          </div>
          <div className="human__legs">
            <div className="human__leg-l"></div>
            <div className="human__leg-r"></div>
          </div>
          <div className="human__shoes">
            <div className="human__shoe-c">
              <div className="human__shoe-l"></div>
            </div>
            <div className="human__shoe-c">
              <div className="human__shoe-r"></div>
            </div>
          </div>
        </div>
        <div className="table">
          <div className="table__top">
            <div className="laptop"></div>
            <div className="cup"></div>
          </div>
          <div className="table__bottom"></div>
          <div className="table__legs">
            <div className="table__leg"></div>
            <div className="table__leg"></div>
            <div className="table__leg"></div>
            <div className="table__leg"></div>
          </div>
        </div>
        <div className="chair">
          <div className="chair__top"></div>
          <div className="chair__line"></div>
          <div className="chair__base"></div>
          <div className="chair__wheels">
            <div className="chair__wheel"></div>
            <div className="chair__wheel"></div>
          </div>
        </div>
        <div className="trashcan"></div>
        <div className="window">
          <div className="window__container">
            <div className="building">
              <div className="building__window building__window--active building__window-on-1"></div>
              <div className="building__window building__window--active building__window-on-2"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window building__window--active building__window-on-3"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window"></div>
            </div>
            <div className="building">
              <div className="building__window"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window building__window--active building__window-on-4"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window building__window--active building__window-on-3"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
            </div>
            <div className="building">
              <div className="building__window-f"></div>
              <div className="building__window-f building__window--active building__window-on-1"></div>
              <div className="building__window-f building__window--active"></div>
              <div className="building__window-f"></div>
              <div className="building__window-f"></div>
              <div className="building__window-f"></div>
              <div className="building__window-f building__window--active"></div>
              <div className="building__window-f"></div>
              <div className="building__window-f"></div>
              <div className="building__window-f building__window--active building__window-on-2"></div>
              <div className="building__window-f building__window--active"></div>
              <div className="building__window-f"></div>
              <div className="building__window-f building__window--active"></div>
              <div className="building__window-f"></div>
            </div>
            <div className="building">
              <div className="building__window"></div>
              <div className="building__window building__window--active building__window-on-1"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window building__window--active building__window-on-4"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window building__window--active"></div>
              <div className="building__window"></div>
              <div className="building__window"></div>
            </div>
          </div>
        </div>
        <div className="lamp">
          <div className="lamp__top">
            <div className="lamp__light"></div>
          </div>
          <div className="lamp__base-c">
            <div className="lamp__base"></div>
          </div>
        </div>
        <div className="bricks">
          <div className="brick"></div>
          <div className="brick"></div>
        </div>
      </div>
    </div>
  );
}

export default Index;
