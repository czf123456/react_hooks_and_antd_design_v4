// 得意的点  把组件 和菜单栏的各个属性组成一个数组对象 暴露
import React from 'react'
import Index from '../ButtonPage/Index';
import Index1 from '../IconPage/Index' ;
import Index2 from '../Fallback/Index' ;
import Index3 from '../UserControl/Index';
import Index4 from '../SampleReels/Index';
import Index5 from '../Comments/Index' ;
import Index6 from '../Wechat/Index' ;
import Index7 from '../BoardCastRoom/Index' 
import Index8 from '../About/Index'

const arr = [
    {
        key:'1',
        title:'按钮',
        content:<Index></Index>
    },
    {
        key:'2',
        title:'图标',
        content:<Index1/>
    },
    {
        key:'3',
        title:'反馈',
        content:<Index2 />
    },
    {
        key:'4',
        title:'用戶管理',
        content:<Index3 />
    },
    {
        key:'5',
        title:'读书会',
        content:<Index4 />
    },
    {
        key:'6',
        title:'评论区',
        content:<Index5 />
    },
    {
        key:'7',
        title:'聊天室',
        content:<Index6 />
    },
    {
        key:'8',
        title:'直播间',
        content:<Index7 />
    },
    {
        key:'9',
        title:'关于',
        content:<Index8 />
    }
]
export default arr ;