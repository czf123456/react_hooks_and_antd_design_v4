import React,{Suspense} from 'react'
const Login = React.lazy(() => import('./Index')) ;
export default function LoginLoading() {
    return (
            <Suspense fallback={<div>等我一会啊</div>}>
                <Login />
            </Suspense>
    )
}
