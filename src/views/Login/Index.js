import React, { useState, useRef, useReducer, Suspense, useEffect } from "react";
import { Button } from "antd";
import ParticleField from "react-particles-webgl";
import "./login.scss";
import LoginForm from "./LoginForm";
import RegisterForm from "./RegisterForm";
import Context from "./context";
import { useHistory } from "react-router-dom";
import flowheart from "../../utils/hearts";
import Loading from "../../components/Loading/RouterLoading";
import loadingimages from "../../utils/loadingImages";
const data = {
  pianyileft: 0,
  tux: 0,
  front: null,
  visible: false,
};
const images = [
  `${process.env.REACT_APP_BASE_URL}/images/robet.jpg`,
  `${process.env.REACT_APP_BASE_URL}/images/883.jpg`,
  `${process.env.REACT_APP_BASE_URL}/images/820.jpg`,
  `${process.env.REACT_APP_BASE_URL}/images/835.jpg`,
];
const imglist = [
  `${process.env.REACT_APP_BASE_URL}/images/f1.jpg`,
  `${process.env.REACT_APP_BASE_URL}/images/f2.jpg`,
  `${process.env.REACT_APP_BASE_URL}/images/f3.jpg`,
  `${process.env.REACT_APP_BASE_URL}/images/f4.jpg`,
];
function changeReducer(state, action) {
  let statee = JSON.parse(JSON.stringify(state));
  if (action.type === "chuanzhi") {
    statee.pianyileft = action.value;
    return statee;
  } else if (action.type === "xTrue") {
    statee.tux = action.value;
    return statee;
  } else if (action.type === "sFront") {
    statee.front = action.ele;
    console.log(statee);
    return statee;
  } else if (action.type === "sVisible") {
    statee.visible = action.value;
    return statee;
  }

  return statee;
}
function Login() {
  const [type, setType] = useState("canvas");
  const [loading, setLoading] = useState(true);
  const containerRem = useRef();
  const [state, dispatch] = useReducer(changeReducer, data);
  const history = useHistory();
  const url = "/images/星空.jpg";
  const loadBigImgAsync = (url) => {
    // 为了提前加载图片
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const image = new Image();
        image.onload = function () {
          resolve(url);
        };
        image.onerror = function () {
          console.log("图片加载错误");
        };
        image.src = url;
      }, 10);
    });
  };
  const config = {
    showCube: false,
    dimension: "3D",
    velocity: 2.5,
    boundaryType: "bounce",
    antialias: true,
    direction: {
      xMin: -1,
      xMax: 1,
      yMin: -1,
      yMax: 1,
      zMin: -1,
      zMax: 1,
    },
    lines: {
      colorMode: "rainbow",
      color: "#fff",
      transparency: 0.9,
      limitConnections: true,
      maxConnections: 30,
      minDistance: 235,
      visible: true,
    },
    particles: {
      colorMode: "rainbow",
      color: "#fff",
      transparency: 1.0,
      shape: "circle",
      boundingBox: type,
      count: 300,
      minSize: 10,
      maxSize: 100,
      visible: true,
    },
    cameraControls: {
      enabled: true,
      enableDamping: true,
      dampingFactor: 0.4,
      enableZoom: true,
      autoRotate: true,
      autoRotateSpeed: 0.3,
      resetCameraFlag: true,
    },
  };
  useEffect(() => {
    flowheart(
      window,
      document,
      undefined,
      window.location.href.substring(window.location.origin.length + 1, window.location.href.length)
    );
    loadBigImgAsync("/images/星空.jpg").then(() => {
      setLoading((prev) => false);
      // setLoading(prev => false) ;
    });
    // loadingimages(imglist) ;
  }, []);
  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <div
          className="login"
          style={{
            background: `url(${url}) no-repeat`,
            height: "100%",
            width: "100%",
            backgroundSize: "cover",
          }}
        >
          <Button
            type="info"
            className="change-effects"
            ghost
            onClick={() => {
              return type === "cube" ? setType("canvas") : setType("cube");
            }}
          >
            Change The Special Effects
          </Button>
          <div className="containerscreen">
            <ParticleField config={config} />
          </div>

          <div className="login-container">
            <Context.Provider value={{ ele: containerRem, dispatch, state }}>
              <div className="login-innercontainer" ref={containerRem}>
                <LoginForm />
                <RegisterForm />
              </div>
            </Context.Provider>
          </div>
        </div>
      )}
    </>
  );
}

export default Login;
