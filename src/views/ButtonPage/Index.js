import React from "react";
import { Card, Row, Col, Button, Tooltip } from "antd";
import { SearchOutlined, DownloadOutlined } from "@ant-design/icons";
import Word from "../../components/Word/Index";
function Index() {
  const gridStyle = {
    width: "25%",
    textAlign: "center",
  };
  return (
    <div>
      <Row>
        <Card.Grid>
          <Word>
            <div className="markdown">
              <h1>何时使用按钮</h1>
              <h2 style={{ margin: "15px 0" }}>按钮用于开始一个即时操作。</h2>
              标记了一个（或封装一组）操作命令，响应用户点击行为，触发相应的业务逻辑。
            </div>
          </Word>
        </Card.Grid>
      </Row>
      <Row gutter={[40, 40]} style={{ marginTop: "40px" }} align="middle">
        <Col span={12}>
          <Card.Grid style={gridStyle}>
            <Button type="primary">Primary</Button> &emsp;&emsp;&emsp;&emsp;
            <Button>Default</Button>&emsp;&emsp;&emsp;&emsp;
            <Button type="dashed">Dashed</Button>&emsp;&emsp;&emsp;&emsp;
            <Button type="link">Link</Button>
          </Card.Grid>
        </Col>
        <Col span={12}>
          <Card.Grid style={gridStyle}>
            <Tooltip title="search">
              <Button type="primary" shape="circle" icon={<SearchOutlined />} />
            </Tooltip>
            &emsp;&emsp;&emsp;&emsp;
            <Button type="primary" shape="circle">
              A
            </Button>
            &emsp;&emsp;&emsp;&emsp;
            <Button type="primary" icon={<SearchOutlined />}>
              Search
            </Button>
            &emsp;&emsp;&emsp;&emsp;
            <Tooltip title="search">
              <Button shape="circle" icon={<SearchOutlined />} />
            </Tooltip>
            &emsp;&emsp;&emsp;&emsp;
            <Button icon={<SearchOutlined />}>Search</Button>
            <br />
            <br />
            <br />
            <Tooltip title="search">
              <Button shape="circle" icon={<SearchOutlined />} />
            </Tooltip>
            &emsp;&emsp;&emsp;&emsp;
            <Button icon={<SearchOutlined />}>Search</Button>&emsp;&emsp;&emsp;&emsp;
            <Tooltip title="search">
              <Button type="dashed" shape="circle" icon={<SearchOutlined />} />
            </Tooltip>
            &emsp;&emsp;&emsp;&emsp;
            <Button type="dashed" icon={<SearchOutlined />}>
              Search
            </Button>
            &emsp;&emsp;&emsp;&emsp;
          </Card.Grid>
        </Col>
      </Row>
      <Row gutter={[40, 40]}>
        <Col span={12}>
          <Card.Grid style={gridStyle}>
            <Button type="primary" size="large" icon={<DownloadOutlined />} />
            &emsp;&emsp;&emsp;&emsp;
            <Button type="primary" shape="circle" icon={<DownloadOutlined />} />
            &emsp;&emsp;&emsp;&emsp;
            <Button type="primary" size="small" shape="round" icon={<DownloadOutlined />} />
            &emsp;&emsp;&emsp;&emsp;
            <br />
            <br />
            <br />
            <Button type="primary" size="large" shape="round" icon={<DownloadOutlined />}>
              Download
            </Button>
            &emsp;&emsp;&emsp;&emsp;
            <Button type="primary" icon={<DownloadOutlined />}>
              Download
            </Button>
          </Card.Grid>
        </Col>
        <Col span={12}>
          <Card.Grid style={gridStyle}>
            <Button type="primary" disabled>
              Primary(disabled)
            </Button> &emsp;&emsp;&emsp;&emsp;
            <Button disabled>Default(disabled)</Button> &emsp;&emsp;&emsp;&emsp;
            <Button type="dashed" disabled>
              Dashed(disabled)
            </Button> &emsp;&emsp;&emsp;&emsp;
            <br/>
            <br/>
            <br/>
            <Button type="link" disabled>
              Link(disabled)
            </Button> &emsp;&emsp;&emsp;&emsp;
            <Button type="link" danger disabled>
              Danger Link(disabled)
            </Button> &emsp;&emsp;&emsp;&emsp;
            <Button danger disabled>
              Danger Default(disabled)
            </Button>
          </Card.Grid>
        </Col>
      </Row>
    </div>
  );
}

export default Index;
