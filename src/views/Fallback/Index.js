import React, { useState, useMemo } from "react";
import {
  Message,
  Result,
  Drawer,
  Button,
  Card,
  Row,
  Col,
  Select,
  Alert,
  Radio,
  Space,
  Modal,
  message,
  notification,
} from "antd";
import Word from "../../components/Word/Index";
const RadioGroup = Radio.Group;
const ReachableContext = React.createContext();
const UnreachableContext = React.createContext();
const config = {
  title: "Use Hook!",
  content: (
    <div>
      <ReachableContext.Consumer>{(name) => `Reachable: ${name}!`}</ReachableContext.Consumer>
      <br />
      <UnreachableContext.Consumer>{(name) => `Unreachable: ${name}!`}</UnreachableContext.Consumer>
    </div>
  ),
};
const { Option } = Select;
function Index() {
  const [placement, setPlayment] = useState("left");
  const [visibledrawer, setvisibledrawer] = useState(false);
  const [visibleModal, setvisibleModal] = useState(false);
  const [modal, contextHolder] = Modal.useModal();
  const [positionValue, setposition] = useState("topRight");
  const [resultvalue, setResultValue] = useState("success");
  const title = useMemo(() => {
    // if (resultvalue == 'success') {
    //   return '祝贺您登录成功'
    // }
    let word = "";
    switch (resultvalue) {
      case "success":
        word = "祝贺您登录成功";
        break;
      case "warning":
        word = "警告,特朗普要发动战争";
        break;
      case "error":
        word = "电脑短路,请拆开电脑查看详情";
        break;
      case "info":
        word = "加油,奥利给";
        break;
      case "404":
        word = "您访问的页面不存在";
        break;
      case "403":
        word = "最近网站遭过多爬虫,需要验证才能登录";
        break;
      case "500":
        word = "服务器维修中...";
        break;
      default:
        word = "没有意外";
        break;
    }
    return word;
  }, [resultvalue]);
  const subTitle = useMemo(() => {
    // if (resultvalue == 'success') {
    //   return '祝贺您登录成功'
    // }
    let word = "";
    switch (resultvalue) {
      case "success":
        word = "交过会员费才能继续,否则返回登录";
        break;
      case "warning":
        word = "歼-20出列,东风导弹发射准备中";
        break;
      case "error":
        word = "在主机上洒水可以请扫灰尘";
        break;
      case "info":
        word = "我里个giao giao";
        break;
      case "404":
        word = "根据国家法律法规,不允许访问此等网站";
        break;
      case "403":
        word = "交钱可以继续爬";
        break;
      case "500":
        word = "买下服务器,发家致富";
        break;
      default:
        word = "没有意外";
        break;
    }
    return word;
  }, [resultvalue]);
  const extraVal = useMemo(() => {
    // if (resultvalue == 'success') {
    //   return '祝贺您登录成功'
    // }
    let word = "";
    switch (resultvalue) {
      case "success":
        word = [
          <Button type="primary" key="console">
            成功续费
          </Button>,
          <Button key="buy">续费超级会员</Button>,
        ];
        break;
      case "warning":
        word = (
          <Button type="primary" key="console">
           入伍当兵
          </Button>
        );
        break;
      case "error":
        word = (
          <Button type="primary" key="console">
            查看具体如何洒水
          </Button>
        );
        break;
      case "info":
        word = [
          <Button type="primary" key="console">
            喜欢giao哥
          </Button>,
          <Button key="buy">喜欢奥利给</Button>,
        ];
        break;
      case "404":
        word = <Button type="primary">返回上一个页面</Button>;
        break;
      case "403":
        word = <Button type="primary">去充钱</Button>;
        break;
      case "500":
        word = <Button type="primary">入侵</Button>;
        break;
      default:
        word = "没有意外";
        break;
    }
    return word;
  }, [resultvalue]);
  const onChangeResult = function (e) {
    setResultValue((prev) => e.target.value);
  };
  const onChangePlacement = function (e) {
    setPlayment((prev) => e.target.value);
  };
  const showDrawer = function () {
    setvisibledrawer((prev) => true);
  };
  const onClosePlayment = function () {
    setvisibledrawer((prev) => false);
  };
  const handleOkModal = function () {
    setvisibleModal((prev) => false);
  };
  const handleCancelModal = function () {
    setvisibleModal((prev) => false);
  };
  const showModal = function () {
    setvisibleModal((prev) => true);
  };
  const onNotiChange = function (value) {
    setposition((prev) => value);
  };
  const openNotificationWithIcon = (type, placement) => {
    notification[type]({
      message: "Notification Title",
      description:
        "This is the content of the notification. This is the content of the notification. This is the content of the notification.",
      placement,
    });
  };
  return (
    <div className="fallback">
      <Row>
        <Card.Grid>
          <Word>
            <div className="markdown">
              <h1>何时使用</h1>
              <h2>用来提示或展示信息</h2>
              <h2>由于反馈组件太多，选择几个常用的组件进行说明：</h2>
              <ul>
                <li>
                  <code>Alert</code>&nbsp;警告提示，展现需要关注的信息。
                </li>
                <li>
                  <code>Drawer</code>&nbsp;屏幕边缘滑出的浮层面板。
                </li>
                <li>
                  <code>Modal</code>&nbsp;模态对话框。
                </li>
                <li>
                  <code>Notification</code>&nbsp;全局展示通知提醒信息。
                </li>
                <li>
                  <code>Message</code>&nbsp;全局展示操作反馈信息。
                </li>
                <li>
                  <code>Result</code>&nbsp;用于反馈一系列操作任务的处理结果。
                </li>
              </ul>
            </div>
          </Word>
        </Card.Grid>
      </Row>
      <Row style={{ marginTop: "20px", marginBottom: "20px" }} gutter={20}>
        <Col span={12}>
          <Card
            title="Alert"
            bordered={false}
            hoverable
            style={{ width: "100%", marginBottom: "20px" }}
          >
            <Alert message="Success Tips" type="success" showIcon />
            <br />
            <Alert message="Informational Notes" type="info" showIcon />
            <br />
            <Alert message="Warning" type="warning" showIcon closable />
            <br />
            <Alert message="Error" type="error" showIcon />
          </Card>
          <Card
            title="Drawer"
            bordered={false}
            hoverable
            style={{ width: "100%", marginBottom: "20px" }}
          >
            <Space>
              <RadioGroup defaultValue={placement} onChange={onChangePlacement}>
                <Radio value="top">top</Radio>
                <Radio value="right">right</Radio>
                <Radio value="bottom">bottom</Radio>
                <Radio value="left">left</Radio>
              </RadioGroup>
              <Button type="primary" onClick={showDrawer}>
                Open
              </Button>
            </Space>
            <Drawer
              title="Basic Drawer"
              placement={placement}
              closable={false}
              onClose={onClosePlayment}
              visible={visibledrawer}
            >
              <p>Some contents...</p>
              <p>Some contents...</p>
              <p>Some contents...</p>
            </Drawer>
          </Card>
        </Col>
        <Col span={12}>
          <Card
            title="Modal"
            bordered={false}
            hoverable
            style={{ width: "100%", marginBottom: "20px" }}
          >
            <ReachableContext.Provider value="Light">
              <Space>
                <Button type="primary" onClick={showModal}>
                  Open Modal
                </Button>
                <Button
                  onClick={() => {
                    modal.confirm(config);
                  }}
                >
                  Confirm
                </Button>
                <Button
                  onClick={() => {
                    modal.warning(config);
                  }}
                >
                  Warning
                </Button>
                <Button
                  onClick={() => {
                    modal.info(config);
                  }}
                >
                  Info
                </Button>
                <Button
                  onClick={() => {
                    modal.error(config);
                  }}
                >
                  Error
                </Button>
              </Space>
              {contextHolder}

              <UnreachableContext.Provider value="Bamboo" />
            </ReachableContext.Provider>
            <Modal
              title="Basic Modal"
              visible={visibleModal}
              onOk={handleOkModal}
              onCancel={handleCancelModal}
            >
              <p>Some contents...</p>
              <p>Some contents...</p>
              <p>Some contents...</p>
            </Modal>
          </Card>
          <Card
            title="Message"
            bordered={false}
            hoverable
            style={{ width: "100%", marginBottom: "20px" }}
          >
            <Space>
              <Button
                type="primary"
                onClick={() => {
                  message.info("This is a info message");
                }}
              >
                Display normal message
              </Button>
              <Button
                onClick={() => {
                  message.success("This is a success message");
                }}
              >
                Success
              </Button>
              <Button
                onClick={() => {
                  message.error("This is an error message");
                }}
              >
                Error
              </Button>
              <Button
                onClick={() => {
                  message.warning("This is a warning message");
                }}
              >
                Warning
              </Button>
              <Button
                onClick={() => {
                  message
                    .loading("Action in progress..", 2.5)
                    .then(() => message.success("Loading finished", 2.5))
                    .then(() => message.info("Loading finished is finished", 2.5));
                }}
              >
                Display sequential messages
              </Button>
            </Space>
          </Card>
          <Card
            title="Notification"
            bordered={false}
            hoverable
            style={{ width: "100%", marginBottom: "20px" }}
          >
            <Space>
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Select a position"
                optionFilterProp="children"
                onChange={onNotiChange}
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                <Option value="topRight">topRight</Option>
                <Option value="bottomRight">bottomRight</Option>
                <Option value="bottomLeft">bottomLeft</Option>
                <Option value="topLeft">topLeft</Option>
              </Select>
              <Button onClick={() => openNotificationWithIcon("success", positionValue)}>
                Success
              </Button>
              <Button onClick={() => openNotificationWithIcon("info", positionValue)}>Info</Button>
              <Button onClick={() => openNotificationWithIcon("warning", positionValue)}>
                Warning
              </Button>
              <Button onClick={() => openNotificationWithIcon("error", positionValue)}>
                Error
              </Button>
            </Space>
          </Card>
          <Row>
            <RadioGroup defaultValue={resultvalue} onChange={onChangeResult}>
              <Radio value="success">success</Radio>
              <Radio value="info">info</Radio>
              <Radio value="warning">warning</Radio>
              <Radio value="403">403</Radio>
              <Radio value="404">404</Radio>
              <Radio value="500">500</Radio>
              <Radio value="error">error</Radio>
            </RadioGroup>
          </Row>
        </Col>
      </Row>
      <Row style={{ marginBottom: "40px" }}>
        <Result
          style={{ margin: "0 auto" }}
          status={resultvalue}
          title={title}
          subTitle={subTitle}
          extra={extraVal}
        />
      </Row>
    </div>
  );
}

export default Index;
