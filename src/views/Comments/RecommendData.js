import React, { useState, useEffect } from "react";
import { Affix, Card, Rate, Divider, Progress, Row, Col, Button, message, Empty } from "antd";
import { CloseCircleOutlined, HeartFilled } from "@ant-design/icons";
import axios from "axios";
import {connect} from 'react-redux';
import Qs from 'qs' ;
const desc = ["bug多", "再接再厉", "普通", "很好", "精彩"];
function RecommendData({UpateUserInfo}) {
  const [rateflag, setrateFlag] = useState(false);
  const [rate,setRate] = useState(0) ;
  const [rateValue, setrateValue] = useState(3);
  const [total, setTotal] = useState();
  const [allrate, setallRate] = useState();
  const [rateshow,setRateshow] = useState(true)
  const handleChange = (val) => {
    setrateValue((prev) => val);
  };
  const postRage = async () => {
    if(rateValue == 0) {
      return message.warning('评分不能低于一颗爱心');
    }
    let {data:res} = await axios.post('/api/postrate',Qs.stringify({
      rate:rateValue,
      userid:UpateUserInfo.userInfo.id,
    }),{
      headers:{
        'Content-type':'application/x-www-form-urlencoded'
      }
    });
    if(res.ok !== 1) {
      return message.error(res.message) ;
    }
    message.success(res.message) ;
    setrateFlag((prev) => true);
  };
  const getCurrentRate = async () => {
    let { data: res } = await axios.get("/api/getcurrentrate",{
      params:{
        userid:UpateUserInfo.userInfo.id,
      }
    });
    if (res.ok < 0) {
      return message.error(res.message);
    }
    if (res.ok == 1) {
      setrateFlag((prev) => true);
      setRate(prev => res.rate) ;
    }
  };
  const getallRate = async() => {
  let {data:res} = await axios.get('/api/allrate',{
    params:{
      userid:UpateUserInfo.userInfo.id,
    }
  });
  if(res.ok !== 1) {
    return message.error(res.message) ;
  }
  setTotal(prev => res.total) ;
  setallRate(prev => res.all);
  }
  const closeRate = () => {
   setRateshow(prev => false)
  }
  useEffect(() => {
    getCurrentRate();
    if(rateflag) {
      getallRate() ;
    }
  }, [rateflag]);
  return (
    <>
   {
    rateshow ?  (<Affix style={{ position: "fixed", top: "250px", right: "48px", zIndex: 100 }}>
     {rateflag ? (
       total&&allrate ? (
         <Card
           title={
             <div className="tophead" style={{ overflow: "hidden", display: "flex" }}>
               {/* <div className="fontnum" style={{ fontSize: "40px", marginRight: "15px" }}>
             9.5
           </div> */}
               <Progress
                 type="circle"
                 strokeColor={{
                   "0%": "#108ee9",
                   "100%": "#87d068",
                 }}
                 percent={( (allrate.one * 1 + allrate.two * 2 +  allrate.three * 3 + allrate.four * 4 + allrate.five * 5) / 5 / total * 100).toFixed(1)}
               />
               <div className="rightrate" style={{ marginTop: "25px", marginLeft: "10px" }}>
                 <Rate disabled character={<HeartFilled />} defaultValue={Math.ceil((allrate.one * 1 + allrate.two * 2 +  allrate.three * 3 + allrate.four * 4 + allrate.five * 5) / 5 / total * 5)} allowHalf={false} />
               <div style={{ color: "#3377aa", fontSize: "13px" ,marginTop:'30px'}}>{total}人评价</div>
               </div>
             </div>
           }
           style={{ width: 350 }}
           extra={<CloseCircleOutlined style={{ fontSize: "20px" }} onClick={closeRate} />}
           hoverable
         >
           <Row gutter={[12, 12]}>
             <Col span={3}>5心</Col>
             <Col span={21}>
               <Progress
                 strokeColor={{
                   from: "#108ee9",
                   to: "#87d068",
                 }}
                 percent={allrate.five ? (allrate.five / total * 100).toFixed(1):0}
                 status="active"
               />
             </Col>
           </Row>
           <Row gutter={[12, 12]}>
             <Col span={3}>4心</Col>
             <Col span={21}>
               <Progress
                 strokeColor={{
                   from: "#108ee9",
                   to: "#87d068",
                 }}
                 percent={allrate.four ? ((allrate.four/ total) * 100).toFixed(1):0}
                 status="active"
               />
             </Col>
           </Row>
           <Row gutter={[12, 12]}>
             <Col span={3}>3心</Col>
             <Col span={21}>
               <Progress
                 strokeColor={{
                   from: "#108ee9",
                   to: "#87d068",
                 }}
                 percent={allrate.three ? (allrate.three/ total * 100).toFixed(1):0}
                 status="active"
               />
             </Col>
           </Row>
           <Row gutter={[12, 12]}>
             <Col span={3}>2心</Col>
             <Col span={21}>
               <Progress
                 strokeColor={{
                   from: "#108ee9",
                   to: "#87d068",
                 }}
                 percent={allrate.two ? (allrate.two / total * 100).toFixed(1):0}
                 status="active"
               />
             </Col>
           </Row>
           <Row gutter={[12, 12]}>
             <Col span={3}>1心</Col>
             <Col span={21}>
               <Progress
                 strokeColor={{
                   from: "#108ee9",
                   to: "#87d068",
                 }}
                 percent={allrate.one ? (allrate.one / total * 100).toFixed(1):0}
                 status="active"
               />
             </Col>
           </Row>
           <div style={{textAlign:'center',marginTop:'10px'}}>
               您的评价: &ensp; <span style={{color:'#1cbf75'}}>{rate}</span>&nbsp;心 （{rate>0?desc[rate-1]:''}）
           </div>
         </Card>
       ) : (
         <Empty description='数据显示中' image={Empty.PRESENTED_IMAGE_SIMPLE} />
       )
     ) : (
       <span>
         <Rate
           tooltips={desc}
           character={<HeartFilled />}
           onChange={handleChange}
           value={rateValue}
         />
         {rateValue ? <div style={{ textAlign: "center" }}>{desc[rateValue - 1]}</div> : ""}
         <div style={{ textAlign: "center", marginTop: "10px" }}>
           <Button type="link" onClick={postRage}>
             提交评分
           </Button>
         </div>
       </span>
     )}
   </Affix>):null
   }
   </>
  );
}

export default connect(state=>state)(RecommendData);
