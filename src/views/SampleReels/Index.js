import React, { useReducer } from "react";
import Book from "../../components/Book/Index";
import { Row, Col } from "antd";
const data = {
  key: null,
};
function reducer(state, action) {
  let stateClone = { ...state };
  stateClone.key = action;
  return stateClone;
}
function Index() {
  const [state, dispatch] = useReducer(reducer, data);
  return (
    <div className="readbook">
      <Row>
        <Col span={8} style={{ display: "flex" }}>
          <Book
            pages={[
             <> 一粒尘可填海，一根草斩尽日月星辰，弹指间天翻地覆。群雄并起，万族林立，诸圣争霸，乱天动地；问苍茫大地，谁主沉浮？一个少年从大荒中走出，一切从这里开始。
             <img src="/images/钻出.jpg" alt="" style={{width:'100%'}}/>
             
             </>,
              `漫长岁月后，也许会有那样一个人，独自站在岁月长河上，回首万古，独伴神道。
              路真的达到尽头了吗？也许还有路，只是不传于世，只有真正达到的人，才会回首一叹，不留一言于世间。`,
              `观空亦空，空无所空，所空既无，无无亦无。不生不灭，云散碧空山色净；无去无来，慧归禅定月轮孤……`,
              `遥望未来，一片混沌；再回首，他身后一片虚无。`,
            ]}
            state={state}
            dispatch={dispatch}
            author="辰东"
            name="圣墟"
            keys={0}
            bookcolor="#a4b8c9"
            backDes={
              <>
                <img src="/images/石昊.jpg" alt="" style={{ width: "100%", height: "100%" }} />
                {/* <p>待到山花烂漫时，</p>
                <p>辰东已成拖更王。</p> */}
              </>
            }
            cover={
              <>
                <img src="/images/完美世界.jpg" alt="" style={{ width: "100%" }} />
                <h2>
                  {/* <span>圣墟</span> */}
                  <span
                    style={{
                      fontSize: "80px",
                      lineHeight: "80px",
                      display: "block",
                      position: "relative",
                      color: "#fff",
                      fontFamily: `'the_godfatherregular', Georgia, serif`,
                      fontWeight: 400,
                    }}
                  >
                    <i>Are you joke me ?</i>
                  </span>
                </h2>
              </>
            }
          ></Book>
        </Col>
        <Col span={8} style={{ display: "flex" }}>
          <Book
            pages={[
              "在破败中崛起，在寂灭中复苏。 沧海成尘，雷电枯竭，那一缕幽雾又一次临近大地，世间的枷锁被打开了，一个全新的世界就此揭开神秘的一角……",
              `艳阳下，藤蔓色彩斑斓，长势旺盛。 

          有的赤红如火，叶片翻动时，赤霞漫天。有的雪白晶莹，微风拂动，叶片如雪花翻卷。有的黑黝黝，宛若墨龙倒挂…… 
      
          每一座较大的城市的上空都有数十根藤蔓，离地从上万米到数百米不等，引发不少人的探索欲望。 
      
          "这到底是什么？"楚风总觉得不妙，这么多藤蔓出现是世界各地，吸引所有人的目光。 
      
          据闻到现在为止最起码有两三千人消失，沿着藤蔓攀爬上去，消失在直径一两米的雾团内。 
      
          有的为异人，更有一些普通人，赌性很大`,
              `，想改变当前自身的状态，迫不及待的闯上高空中。 
      
          黄牛狐疑，仔细看了又看，道："有点像千星藤。"`,
            ]}
            author="辰东"
            name="圣墟"
            bookcolor="#1d2c3e"
            state={state}
            dispatch={dispatch}
            keys={1}
            backDes={
              <>
                <p>待到山花烂漫时，</p>
                <p>辰东已成拖更王。</p>
              </>
            }
            cover={
              <>
                <img src="/images/圣墟.jpg" alt="" style={{ width: "100%" }} />
                <h2>
                  <span>圣墟</span>
                  <span>作者：辰不更</span>
                </h2>
              </>
            }
          ></Book>
        </Col>
        <Col span={8} style={{ display: "flex" }}>
          <Book
            pages={[
              `This brain-friendly guide teaches you everything from JavaScript language fundamentals to advanced topics, including objects, functions, and the browser’s document object model. You won’t just be reading—you’ll be playing games, solving puzzles, pondering mysteries, and interacting with JavaScript in ways you never imagined. And you’ll write real code, lots of it, so you can start building your own web`,
              `applications. Prepare to open your mind as you learn (and nail) key topics including.`
            ]}
            author="chenzefan"
            name="javascript"
            bookcolor="#000"
            state={state}
            dispatch={dispatch}
            keys={3}
            backDes={
              <>
                <p>Eric Freeman is described by Head First series co-creator Kathy Sierra as “one of those rare individuals fluent in the language, practice, and culture of multiple domains from hipster hacker, to corporate VP, engineer, think tank." Professionally, Eric recently ended nearly a decade as a media company executive, having held the position of CTO of Disney Online</p>
                
              </>
            }
            cover={
              <>
                <img src="/images/百度.gif" alt="" style={{ width: "100%" }} />
                <h1 style={{color:'white',padding:'0 20px',fontSize:'40px',fontFamily: 'the_godfatherregular, Georgia, serif',fontStyle:'italic'}}>论学javascript的最终归宿</h1>
              </>
            }
          ></Book>
        </Col>
      </Row>
    </div>
  );
}

export default Index;
