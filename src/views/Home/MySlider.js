import React, { useRef } from "react";
import { Layout, Menu, Avatar } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  AntDesignOutlined,
  BookOutlined,
  UploadOutlined,
  WechatOutlined,
  MessageOutlined,
  InfoCircleOutlined
} from "@ant-design/icons";
import { connect } from "react-redux";
const { SubMenu } = Menu;
function MySlider({ collapsed, changeContent,UpateUserInfo }) {
  // const [whichchoose, setwhichchoose] = React.useState([]);
  const chooseRef = useRef();
  React.useEffect(() => {
    return () => {};
  }, []);
  const changeVal = function (item) {
    changeContent({ type: "select", key: item.key });
    if(UpateUserInfo.allChoosed.indexOf(item.key) == -1) {
      changeContent({type:'addchoose',key:item.key})
    }
  };
  return (
    <>
      <div
        className="avator"
        style={{ height: collapsed ? "76px" : "65px", transition: "all 0.3s", cursor: "pointer" }}
      >
        <img
          src="/images/logo.png"
          alt=""
          style={{
            width: "200px",
            transform: collapsed ? "scale(1.25)" : "scale(1)",
            transition: "all 0.3s",
            transformOrigin: "left top",
          }}
        />
      </div>
      <Menu
        theme={UpateUserInfo.theme}
        mode="inline"
        ref={chooseRef}
        onSelect={changeVal}
        selectedKeys={UpateUserInfo.siderChoose}
      >
        <SubMenu
          key="sub3"
          title={
            <span>
              <AntDesignOutlined />
              <span>antd</span>
            </span>
          }
        >
          <Menu.Item key="1">按钮</Menu.Item>
          <Menu.Item key="2">图标</Menu.Item>
          <Menu.Item key="3">反馈</Menu.Item>
        </SubMenu>
        <Menu.Item key="4">
          <UserOutlined />
          <span>用户管理</span>
        </Menu.Item>
        <Menu.Item key="5">
          <BookOutlined />
          <span>读书会</span>
        </Menu.Item>
        <Menu.Item key="6">
          <MessageOutlined />
          <span>评论区</span>
        </Menu.Item>
        <Menu.Item key="7">
          <WechatOutlined />
          <span>聊天室</span>
        </Menu.Item>
        <Menu.Item key='8'>
          <VideoCameraOutlined />
          <span>直播间</span>
        </Menu.Item>
        <Menu.Item key="9">
          <InfoCircleOutlined />
          <span>关于</span>
        </Menu.Item>
      </Menu>
    </>
  );
}

export default connect(state=>state, (dispatch) => {
  return {
    changeContent: (data) => dispatch(data)
  };
})(MySlider);
