import React, { useMemo } from "react";
import { Tabs,Carousel } from "antd";
import { connect } from "react-redux";
import Swiper from "../../components/Swiper/Index";
import arr from "../item_page_com/Index";
const imgs = [
  `${process.env.REACT_APP_BASE_URL}/images/robet.jpg`,
  `${process.env.REACT_APP_BASE_URL}/images/883.jpg`,
  `${process.env.REACT_APP_BASE_URL}/images/820.jpg`,
  `${process.env.REACT_APP_BASE_URL}/images/835.jpg`,
];
const { TabPane } = Tabs;
function MyContent({ UpateUserInfo, changeContent}) {
  const panes = useMemo(() => {
    let panesed = [];
    UpateUserInfo.allChoosed.forEach((item) => {
      let res = arr.filter((item1) => item1.key === item);
      panesed.push(res[0]);
    });
    return panesed;
  }, [[...UpateUserInfo.allChoosed]]); // 必须把传的数据进行深拷贝  否则无反映 感觉是传进来的redux数据 必须进行一次数据变量重新定义赋值 否则只有初始化时定义赋值
  // const [activeKey,setactiveKey] = useState(panes[0].key)
  const onChange = (activeKey) => {
    // setactiveKey(prev => activeKey)
    changeContent({ type: "select", key: activeKey });
  };
  const obj = {
    remove: function (targetKey) {
      if (UpateUserInfo.allChoosed.length === 1) {
        changeContent({ type: "clearselect" });
        return;
      }
      if (targetKey !== UpateUserInfo.siderChoose[0]) {
        changeContent({ type: "delchoose", key: targetKey });
        return ;
      }
      let index = UpateUserInfo.allChoosed.findIndex((item) => item === targetKey);
      if (index !== 0) {
        changeContent({ type: "select", key: UpateUserInfo.allChoosed[index - 1] });
      } else {
        changeContent({ type: "select", key: UpateUserInfo.allChoosed[index + 1] });
      }
      changeContent({ type: "delchoose", key: targetKey });
    },
  };
  const onEdit = (targetKey, action) => {
    obj[action](targetKey);
  };
  React.useEffect(() => {
    // console.log(UpateUserInfo.allChoosed);
  }, [UpateUserInfo.allChoosed]);
  return (
    <div style={{ height: "100%", width: "100%" }}>
      {UpateUserInfo.allChoosed.length != 0 ? (
        <Tabs
          onChange={onChange}
          hideAdd
          activeKey={UpateUserInfo.siderChoose[0]}
          type="editable-card"
          onEdit={onEdit}
        >
          {panes.map((pane) => (
            <TabPane tab={pane.title} key={pane.key} closable={pane.closable} >
              <div className="tabPane-box" style={{backgroundColor:pane.key==='9'?'black':''}}>
                <div style={{ padding: "24px" }}>{pane.content}</div>
              </div>
            </TabPane>
          ))}
        </Tabs>
      ) : (
    //     <div style={{ height: "100%", width: "100%" }} className="ccc" >
    //   <Carousel autoplay>
    //     {imgs.map((item,index) => {
    //       return (
    //         <div key={index}>
    //           <img src={item} alt="" />
    //         </div>
    //       );
    //     })}
    //   </Carousel>
    // </div>
    //   )}
    // </div>
    <Swiper></Swiper>
  )}</div>)
};

export default connect(
  (state) => state,
  (dispatch) => {
    return {
      changeContent: (data) => dispatch(data)
    };
  }
)(MyContent);
