import React, { useState, useCallback, useEffect } from "react";
import { Row, Col, Button, message } from "antd";
import { connect } from "react-redux";
import ColorPicker from "../../components/ColorPicker/index";
import "./Myheader.scss";
import Avatars from './Avatar'
function MyHeader({ toggle, UpateUserInfo, addPatch,changeTheme }) {
  const [color, setColor] = useState("#13C2C2");
  const [isShow, setiShow] = useState(false);
  const changeColor = function (color) {
    setColor(color);
  };
  const changeTh = useCallback(() => {
    let theme = UpateUserInfo.theme ;
    changeTheme({ type: "changetheme", value: UpateUserInfo.theme == "dark" ? "light" : "dark" });
    localStorage.setItem('theme',theme== "dark" ? "light" : "dark" );
    message.success('主题修改成功')
  }, [UpateUserInfo.theme]);
  useEffect(() => {
    window.document.body.onclick = function (e) {
      // console.log(e.target) ;
      if (e.target !== document.querySelector('.changeTheme')&& e.target !==document.querySelector('.containPicker') ) {
        setiShow((prev) => false);
      }
      
    };
  }, []);
  return (
    <div className="myheader">
      <Row>
        <Col span={16}>
          <svg
            className="icon"
            aria-hidden="true"
            style={{ fontSize: "26px", cursor: "pointer", marginLeft: 20 + "px" }}
            onClick={toggle}
          >
            <use xlinkHref="#icon-shousuocaidan"></use>
          </svg>
        </Col>
        <Col span={8}>
          <Row>
            <Col span={10} style={{ zIndex: 1 }}>
              <div className="changeTheme" style={{backgroundColor:UpateUserInfo.theme == 'dark'?'white':'#001529',transition:'all .2s' }} onClick={()=>{
                setiShow((prev) => true);
              }}>
                <div className="colorshow"></div>
                <div className="containPicker" style={{ display: isShow ? "block" : "none"}}>
                  <ColorPicker color={color} onChange={changeColor}></ColorPicker>
                </div>
              </div>
            </Col>
            <Col span={6}>
            <div className="themechange" onClick={changeTh}>
                  {UpateUserInfo.theme == "dark" ? (
                    <svg
                      className="icon"
                      aria-hidden="true"
                      style={{ fontSize: '40px', cursor: "pointer" }}
                    >
                      <use xlinkHref="#icon-yueliang"></use>
                    </svg>
                  ) : (
                    <svg
                      className="icon"
                      aria-hidden="true"
                      style={{ fontSize: '40px', cursor: "pointer" }}
                    >
                      <use xlinkHref="#icon-qingtian"></use>
                    </svg>
                  )}
                </div>
            </Col>
            <Col span={7} className='avatrs' >
            <Avatars></Avatars>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
}

export default connect(
   state=>state,
  (dispatch) => {
    return {
      addPatch: () => {
        return dispatch({ type: "Adduser" });
      },
      changeTheme:(action)=> {
        return dispatch(action);
      },
    };
  }
)(MyHeader);
