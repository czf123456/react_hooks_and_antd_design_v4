import React from "react";
import { Menu } from "antd";
const { SubMenu } = Menu;
function MenuList() {
  return (
    <Menu>
      <Menu.ItemGroup title="用户中心">
        <Menu.Item key='0'>1st menu item</Menu.Item>
        <Menu.Item key='1'>2nd menu item</Menu.Item>
        <Menu.Item key='3'>1st menu item</Menu.Item>
      </Menu.ItemGroup>
      <Menu.ItemGroup title="设置中心">
        <Menu.Item key='4'>1st menu item</Menu.Item>
        <Menu.Item key='5'>2nd menu item</Menu.Item>
      </Menu.ItemGroup>
    </Menu>
  );
}

export default MenuList;
