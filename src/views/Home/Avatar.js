import React,{useCallback,useEffect} from "react";
import UserInfoEdit from "./UserInfoEdit" ;
import {Avatar,Dropdown,Menu,message,Modal,notification} from 'antd'
import PasswordEdit from './PasswordEdit' ;
import {UserOutlined,DownOutlined,HighlightFilled,SettingTwoTone,SettingOutlined,FullscreenOutlined,LoginOutlined,FullscreenExitOutlined,ExclamationCircleOutlined} from '@ant-design/icons';
import {requestFullScreen,exitFull,isFullscreenForNoScroll} from "../../utils/fullscreen";
import {connect} from 'react-redux';
import {useHistory} from 'react-router-dom'
const { confirm } = Modal;
const {SubMenu} = Menu;
function Avatars({UpateUserInfo,changeConetnt,initWebsocket}) {
  const [visible,setVisible] = React.useState(false) ; 
  const [passwordVisible,setPassword] = React.useState(false) ;
  const [screen,setScreen] = React.useState(false) ;
  const history = useHistory() ;
  const logout = useCallback(()=>{
    confirm({
      title: '您确定要退出登录吗',
      icon: <ExclamationCircleOutlined />,
      content: '退出后需要重新登录',
      onOk() {
        localStorage.removeItem('mysystemtoken');
        changeConetnt({type:'getuser',data:null})
        history.push('/login') ;
        window.less
        .modifyVars({
          "@primary-color":'#69b7ff',
        })
        .then(() => {

        })
        .catch((error) => {
          // message.error(`Failed to update theme`);
        });

        notification['success']({
          message:'提示',
          description:'已经成功退出登录'
        });
        initWebsocket.websocket.close() ;
        changeConetnt({type:"closesocket"});
        changeConetnt({ type: "issystemon", flat: '0' });
        changeConetnt({type:'logout'}) ;
        changeConetnt({type:'conlog'}) ;
      },
      onCancel() {
        message.info('取消退出');
        Modal.destroyAll();
      },
    });
   
  },[])
  const restartTheme = function() {
    changeConetnt({type:'changetheme',value:'dark'}) ;
    localStorage.setItem('theme','dark');
    message.success('恢复默认主题成功')
  } ;
  useEffect(()=>{
    // window.onresize=function() {
    //   console.log(1) ;
    //   setScreen(prev => !prev)
    // }
  },[])
  return (
    <Menu  mode="horizontal">
         <SubMenu  title={<> <Avatar shape="square" size="large" icon={ <img src={UpateUserInfo.userInfo!== null ?UpateUserInfo.userInfo.img:''} ></img> } />&ensp; <span style={{color:'#e5e510'}}>{UpateUserInfo.userInfo!== null? UpateUserInfo.userInfo.username:""} </span></>}>
          <Menu.ItemGroup title="用户中心">
            <Menu.Item key="1" onClick={()=> {setVisible(true)}}><UserOutlined />编辑个人信息</Menu.Item>
            <Menu.Item key="2" onClick={()=> {setPassword(true)}}><SettingTwoTone />修改密码</Menu.Item>
            <Menu.Item key="3" onClick={logout}><LoginOutlined />退出登录</Menu.Item>
          </Menu.ItemGroup>
          <Menu.ItemGroup title="设置中心">
            <Menu.Item key="4" onClick={()=>{
              // setScreen(prev => !prev)
              // if(!screen) {
                requestFullScreen() ;
              // }else {
              //   exitFull() ;
              // }
            }}>
               <><FullscreenOutlined/>切换全屏</>
     
            </Menu.Item>
            <Menu.Item key="5" onClick={restartTheme}><HighlightFilled />恢复默认主题</Menu.Item>
          </Menu.ItemGroup>
        </SubMenu>
        <UserInfoEdit visible={visible} setVisible={setVisible}   />
        <PasswordEdit passwordVisible={passwordVisible} setPassword={setPassword}   />
    </Menu>
  );
}

export default connect(state=>state,(dispatch)=> {
  return {
    changeConetnt:(data) => dispatch(data)
  }
})(Avatars);
