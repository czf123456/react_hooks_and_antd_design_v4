import React, { useState, useMemo } from "react";
import Word from "../../components/Word/Index";
import { Card, Row, Button, Radio, Col, message, Input ,Result} from "antd";
import { BorderOutlined, RightSquareFilled, Html5TwoTone } from "@ant-design/icons";
import { iconlist } from "../../components/Icons/Index";
import copy from "copy-to-clipboard";
const { Search } = Input;
function Index() {
  const [num, setNum] = useState("0");
  const [copyword, setCopyword] = useState("");
  const [sameText, setsameText] = useState("");
  const [enword, setenword] = useState("");
  const handleSizeChange = function (e) {
    setNum((prev) => e.target.value);
  };
  const arrs = useMemo(() => {
    let arr = [];
    iconlist[num - 0].kinds.forEach((item) => {
      let arrinner = [];
      item.icons.forEach((item1) => {
        if (item1.text.toLowerCase().indexOf(enword.toLowerCase()) !== -1) {
          arrinner.push(item1);
        }
      });
      if (arrinner.length > 0) {
        arr.push({
          title: item.title,
          icons: arrinner,
        });
      }
    });
    return arr;
  }, [num, enword]);
  const getCopy = function (ele) {
    copy("<" + ele + " />");
    setCopyword((prev) => "copyied");
    setsameText((prev) => ele);
    message.success(
      <span>
        <code>{"<" + ele + " />"}</code>&nbsp;复制成功&nbsp;🎉
      </span>
    );
    setTimeout(() => {
      setCopyword((prev) => "");
    }, 2000);
    //    var clipboard = new ClipboardJS(e.target) ;
    //   clipboard.on('success',function(ev){
    //       console.log('复制成功');
    //       clipboard.listener.destroy() ;
    //   })
  };
  return (
    <div className="iconlist">
      <Row>
        <Card.Grid>
          <Word>
            <div className="markdown">
              <h1>
                Icon<span class="subtitle">图标</span>
                <a
                  class="edit-button"
                  href="https://github.com/ant-design/ant-design/edit/master/components/icon/index.zh-CN.md"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <span role="img" aria-label="edit" class="anticon anticon-edit">
                    <svg
                      viewBox="64 64 896 896"
                      focusable="false"
                      class=""
                      data-icon="edit"
                      width="1em"
                      height="1em"
                      fill="currentColor"
                      aria-hidden="true"
                    >
                      <path d="M257.7 752c2 0 4-.2 6-.5L431.9 722c2-.4 3.9-1.3 5.3-2.8l423.9-423.9a9.96 9.96 0 000-14.1L694.9 114.9c-1.9-1.9-4.4-2.9-7.1-2.9s-5.2 1-7.1 2.9L256.8 538.8c-1.5 1.5-2.4 3.3-2.8 5.3l-29.5 168.2a33.5 33.5 0 009.4 29.8c6.6 6.4 14.9 9.9 23.8 9.9zm67.4-174.4L687.8 215l73.3 73.3-362.7 362.6-88.9 15.7 15.6-89zM880 836H144c-17.7 0-32 14.3-32 32v36c0 4.4 3.6 8 8 8h784c4.4 0 8-3.6 8-8v-36c0-17.7-14.3-32-32-32z"></path>
                    </svg>
                  </span>
                </a>
              </h1>
              <h2>
                语义化的矢量图形。使用图标组件，你需要安装 <code>@ant-design/icons</code>{" "}
                图标组件包：
              </h2>
              <pre class="language-bash">
                <code>
                  <span class="token function">npm</span>{" "}
                  <span class="token function">install</span> --save @ant-design/icons
                </code>
              </pre>
              <h1 id="设计师专属" data-scrollama-index="0">
                <span>设计师专属</span>
              </h1>
              <p>
                安装{" "}
                <a href="https://kitchen.alipay.com">
                  Kitchen Sketch 插件 <span>💎</span>
                </a>
                ，就可以一键拖拽使用 Ant Design 和 Iconfont 的海量图标，还可以关联自有项目。（仅限于
                Mac 苹果笔记本）
              </p>
            </div>
          </Word>
        </Card.Grid>
      </Row>
      <Row style={{ marginTop: "20px", marginBottom: "20px" }}>
        <Card.Grid>
          <h1 className="icontitle">图标列表</h1>
          <Row>
            <Col span={5}>
              <Radio.Group value={num} size="large" onChange={handleSizeChange} buttonStyle="solid">
                <Radio.Button value="0">
                  <BorderOutlined />
                  &nbsp;线框风格
                </Radio.Button>
                <Radio.Button value="1">
                  <RightSquareFilled />
                  &nbsp;实底风格
                </Radio.Button>
                <Radio.Button value="2">
                  <Html5TwoTone />
                  &nbsp;双色风格
                </Radio.Button>
              </Radio.Group>
            </Col>
            <Col span={13}>
              <Search
                placeholder="在此搜索图标, 点击图标可复制代码（请输入英文字母）"
                size="large"
                onSearch={(value) => setenword(value)}
                onChange={(e) => setenword(e.target.value)}
                enterButton
              />
            </Col>
          </Row>
        </Card.Grid>
      </Row>
      {arrs.length > 0 ? (
        arrs.map((item, index) => {
          return (
            <Row key={index} style={{ marginBottom: "40px", marginTop: "40px" }}>
              <Card title={item.title} style={{ width: "100%" }}>
                <Row>
                  {item.icons.map((item, index1) => {
                    return (
                      <Col
                        key={index1}
                        span={6}
                        style={{ cursor: "pointer" }}
                        onClick={() => {
                          getCopy(item.text);
                        }}
                      >
                        <Card.Grid
                          style={{ textAlign: "center" }}
                          className={`hovercolor ${item.text === sameText && copyword}`}
                        >
                          <div id="">{item.com}</div>
                          <div style={{ marginTop: "15px" }}>{item.text}</div>
                        </Card.Grid>
                      </Col>
                    );

                    //   </ReactClipboard>
                  })}
                </Row>
              </Card>
            </Row>
          );
        })
      ) : (
        <Result
          title="图标不全,请去antd官网查询"
          extra={
            <Button type="primary" key="console" onClick={()=>{window.open('https://ant.design/components/icon-cn/','_blank')}}>
              蚂蚁设计入口
            </Button>
          }
        />
      )}
    </div>
  );
}

export default Index;
