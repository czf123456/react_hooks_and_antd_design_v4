import React, { useEffect } from "react";
import { Result, Button } from "antd";
let player;
function Player({ source }) {
  // const {source} = props ;
  useEffect(() => {
    if (source != null) {
      // eslint-disable-next-line
      player = new Aliplayer(
        {
          id: "player-con",
          // 拉流域名
          source: source,
          width: "100%",
          height: "100%",
          autoplay: true,
          isLive: false,
          rePlay: false,
          playsinline: true,
          preload: true,
          controlBarVisibility: "click",
          useH5Prism: true,
        },
        function (player) {
          console.log("The player is created");
        }
      );
      function endedHandle() {
        var newPlayAuth = "";
        player.dispose(); //销毁
        document.querySelector("#player-con").empty(); //id为html里指定的播放器的容器id
        //重新创建
        // eslint-disable-next-line
        player = new Aliplayer({
          id: "J_prismPlayer",
          autoplay: true,
          source: source,
          playsinline: true,
          useFlashPrism: true,
        });
      }
      var _video = document.querySelector("video");

      player.on("play", function (e) {
        _video.removeEventListener("click", play);
        _video.addEventListener("click", pause);
      });
      player.on("pause", function (e) {
        _video.removeEventListener("click", pause);
        _video.addEventListener("click", play);
      });

      function play() {
        if (player) player.play();
      }

      function pause() {
        if (player) player.pause();
      }
      player.on("ended", endedHandle);
    }
  }, [source]);
  return (
    <div style={{ width: "100%", height: "600px", position: "relative" }}>
      {source == null ? (
        <div
          className="cover-player"
          style={{
            height: "100%",
            width: "100%",
            backgroundColor: "hsl(240, 0%, 44%)",
            position: "absolute",
            top: 0,
            zIndex: 10,
          }}
        >
          {" "}
          <div style={{ display: "none" }}>{source}</div>
          <Result
            style={{
              position: "absolute",
              left: "50%",
              top: "50%",
              transform: "translate(-50%,-50%)",
            }}
            title="主播正在赶来的路上..."
            extra={
              <Button type="primary" key="console">
                直播暂未开启
              </Button>
            }
          />
        </div>
      ) : (
        <div
          className="prism-player"
          id="player-con"
          style={{ display: source == null ? "none" : "block" }}
        ></div>
      )}
    </div>
  );
}

export default Player;
