import React, { useEffect, useState } from "react";
import "./Broadcast.scss";
import { Button, message, Modal, Alert } from "antd";
import Player from "./Player";
import axios from "axios";
import { connect } from "react-redux";
import { ExclamationCircleOutlined } from "@ant-design/icons";
function Index({ UpateUserInfo }) {
  const [source, setSourse] = useState(null);
  const [pushdomain, setpushDomain] = useState();
  const getLive = async function () {
    const { data: res } = await axios.get("/api/getlive", {
      params: {
        userid: UpateUserInfo.userInfo.id,
      },
    });
    if (res.ok == 0) {
      return message.error("发生了异常错误");
    }
    Modal.confirm({
      title: "请确认OBS已经开始推流再按确定,否则有错误",
      icon: <ExclamationCircleOutlined />,
      content: (
        <div>
          <p>推流地址: {res.liveurls.pushurl}</p>
          <p>播放地址: {res.liveurls.pullurl}</p>
        </div>
      ),
      okText: "是",
      okType: "danger",
      cancelText: "否",
      onOk: () => {
        setSourse((preb) => res.liveurls.pullurl);
        Modal.destroyAll();
      },
      onCancel() {
        console.log("cancel");
      },
    });
    console.log(res);
  };
  const endlive = function () {
    Modal.confirm({
      title: "提示",
      icon: <ExclamationCircleOutlined />,
      content: "确认结束直播吗",
      okText: "是",
      okType: "danger",
      cancelText: "否",
      onOk: () => {
        setSourse((preb) => null);
        document.querySelector("object").remove();
        document.querySelector('.operators').remove() ;
        Modal.destroyAll();
      },
      onCancel() {
        console.log("cancel");
      },
    });
  };
  useEffect(() => {}, []);
  return (
    <div className="broadcast" id="components-alert-demo-description">
      <Alert
        message="直播中不要按暂停"
        description="aliplayer本身的问题,在网页中直播按暂停就会显示直播异常,无法恢复！！！"
        type="warning"
        showIcon
      />
      <Alert
        style={{ marginTop: "20px" }}
        message="提示"
        description="按结束按钮重新开启直播没有问题！！！"
        type="success"
        showIcon
        closable
      />

      <Button
        type="default"
        size="large"
        style={{ marginBottom: "20px", marginRight: "50px", marginTop: "20px" }}
        onClick={getLive}
      >
        获取推流和播流
      </Button>
      <Button type="danger" size="large" onClick={endlive} disabled={source == null ? true : false}>
        结束直播
      </Button>
      <Player source={source} />
    </div>
  );
}

export default connect((state) => state)(Index);
