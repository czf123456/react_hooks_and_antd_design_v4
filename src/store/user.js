const state = {
  userInfo: null,
  siderChoose: [],
  theme: "dark",
  allChoosed: [],
  history: null,
  sysconnecting: "0",
};

function UpateUserInfo(preState = state, action) {
  let res = { ...preState };
  if (action.type === "Adduser") {
    res.userInfo = "嘿嘿額";
    return res;
  } else if (action.type === "select") {
    res.siderChoose[0] = action.key;
    return res;
  } else if (action.type === "changetheme") {
    res.theme = action.value;
  } else if (action.type === "addchoose") {
    res.allChoosed.push(action.key);
  } else if (action.type === "delchoose") {
    res.allChoosed = res.allChoosed.filter((item) => item !== action.key);
    // console.log(res) ;
  } else if (action.type === "clearselect") {
    res.siderChoose = [];
    res.allChoosed = [];
  } else if (action.type === "getuser") {
    res.userInfo = action.data;
  } else if (action.type === "getHistory") {
    res.history = action.history;
  } else if (action.type ==="issystemon") {
    res.sysconnecting = action.flat;
  }else if (action.type === 'logout') {
    res =  {
      userInfo: null,
      siderChoose: [],
      theme: "dark",
      allChoosed: [],
      history: null,
      sysconnecting: "0",
    } ;
  }
  return res;
}
export default UpateUserInfo;
