import { createStore,combineReducers,applyMiddleware} from 'redux' ;
import UpateUserInfo from './user' ;
import {chatInfo,initWebsocket} from './chat' ;
import thunk from 'redux-thunk';
const reducer = combineReducers(
    {
       UpateUserInfo,
       chatInfo,initWebsocket
    }
)

export default createStore(reducer,applyMiddleware(thunk)) ;