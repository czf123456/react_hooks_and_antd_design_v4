import { notification, Avatar } from "antd";
import axios from "axios";
import io from "socket.io-client";
const state = {
  chatList: [],
  chatroomid: null,
  isconnected: false,
};
const websocket = {
  websocket: null,
};
const rooms = ["聊天1群", "聊天2群", "智能对话"];
function setWebsocket(websocket) {
  return {
    websocket,
    type: "SetWebsocket",
  };
}
export function chatInfo(prevState = state, action) {
  let state = JSON.parse(JSON.stringify(prevState));
  switch (action.type) {
    case "addList":
      state.chatList = [...state.chatList, action.data];
      return state;
    case "init":
      state.chatList = action.data;
      return state;
    case "chooseId":
      state.chatroomid = action.id;
      return state;
    case "connected":
      state.isconnected = action.isconnected;
      return state;
    case "conlog":
      state = {
        chatList: [],
        chatroomid: null,
        isconnected: false,
      };
      return state;
    default:
      return state;
  }
}

export function initWebsocket(prevState = websocket, action) {
  let state = prevState;
  if (action.type === "setwebsocket") {
    state.websocket = action.websocket;
    return state;
  } else if (action.type === "closesocket") {
    state.websocket = null;
    return state;
  }
  return state;
}
// // const websocket = new WebSocket("ws://" + '169.254.235.97');
// // 初次连接通讯服务
// console.log(user);
// websocket.onopen = function () {
//   const data = {
//     id: user.id,
//     username: user.username,
//     avatar: user.img,
//   };
//   console.log('作用');
//   websocket.send(JSON.stringify(data));
// };
// // 监听服务器的消息事件
// websocket.onmessage = function (event) {
//   const data = JSON.parse(event.data);
//   if (data.type == 0) {
//    notification.info({
//      message:'提示',
//      description:data.message.username + '进入了' + rooms[data.message.chatroomid]
//    })
//   }
//   // 聊天的消息
//   if (data.type == 1) {
//     dispatch(addChat(data.message));
//     notification.open({
//       message: data.message.uername,
//       description: "",
//       icon: <Avatar src={data.message.img}></Avatar>,
//     });
//   }
//   console.log(data);
// };
// dispatch(setWebsocket(websocket));
// dispatch(initChatList());
