import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import "antd/dist/antd.css";
import "./index.scss";
import {ConfigProvider} from 'antd'
import cn from 'antd/es/locale/zh_CN';
import axios from 'axios' ;
import Router from "./router";
import {Provider} from 'react-redux' ;
import store from "./store/index";

// import "./static/iconfont/iconfont.css";


axios.defaults.baseURL = 'http://www.chenzefan.xyz:7001/';
axios.defaults.withCredentials = true;
axios.interceptors.request.use(config => {
  let token = localStorage.getItem('mysystemtoken') ;
  if (token !== null) {
    // 添加请求头 token 验证 
    config.headers.Authorization = 'Bearer ' + token ;
  }
  return config
})
ReactDOM.render(
  <ConfigProvider locale={cn}>
        <Provider store={store}>
  <BrowserRouter>
    <Router></Router>
  </BrowserRouter>
  </Provider>
  </ConfigProvider>,
  document.getElementById("root")
);
