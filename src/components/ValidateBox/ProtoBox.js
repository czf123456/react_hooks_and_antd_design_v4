import React, { useState, useEffect, useRef } from "react";
import {drawBubble} from '../../utils/canvasDraw';
function ProtoBox({ info }) {
  const [infor, setInfor] = useState("1351551");
  // const [canvaser,setCan] = useState({})
  const canvaser = useRef();
  // 获取需要渲染的canvas
  const drawCanvas = () => {
    const ctx = canvaser.current.getContext("2d");
    const width = getWidth();
    ctx.clearRect(0, 0, width, 100);
    ctx.strokeStyle = "#ff0000";

    drawBubble(ctx, 1, 0, width, 70, info);
  };
  // 获取所需的字符长度确定canvas画布的宽度
  const getWidth = () => {
    return info === undefined ? 40 + 9 *18 : 40 + info.length * 18; // 根据字符的fontsize自行决定
  };
  useEffect(() => {
    setInfor((prev) => {
        if (info === undefined) {
          return "";
        }
    //   为的是 如果info不变就不需要重新渲染
        if (info !== "" && prev !== info) {
      drawCanvas();
        }
      return info;
    });
    return () => {};
  }, [info]);
  return (
    <div>
      <canvas
        style={{ color: "red" }}
        height="100"
        width={getWidth()}
        id="cc"
        ref={canvaser}
      ></canvas>
    </div>
  );
}

export default ProtoBox;
