import React from 'react'
import './loading.scss';
function RouterLoading() {
    return (
       <div className='loadingContainer'>
          <div className="container">
  <div className="icecream">
    <div className="flavor">
      <div className="flavours"></div>
    </div>
    <div className="stick"></div>
  </div>
</div>
       </div>
    )
}

export default RouterLoading
