import React,{useEffect,useRef} from 'react'
import { RotateRightOutlined } from '@ant-design/icons';

function Index({children}) {
     const word = children ;
     const refs = useRef() ;
     // 得到一个数组
     const getArr = function(ele,val) {
         let arr = [] ;
         for(let i = 0 ; i < ele.length ; i++) {
             if(typeof ele[i] == 'string' ) {
                 if(ele[i] == '💎') { // 配置自己想要的彩色图标  自己去添
                    arr = ['💎']  
                    console.log(arr) ;
                 }else {
                    arr = arr.concat(ele[i].split('')) ;
                 }
                 
             }else if (typeof ele[i] == 'object') {
                 let val = [] ;
                 let nextEle = []
                 if ((ele[i].props.children instanceof Array) == false) { // 最坑的点 Array.from 在 这里中无效 除非使用语法转换器配置
                    nextEle = [ele[i].props.children]
                 }else {
                     nextEle = ele[i].props.children
                 }
                 val = getArr(nextEle,val) ;
                 let dom ;
                 let svgs = ['rect','circle','ellipse','line','polyine','polygon','path','svg','text','g','use','defs','filter','feOffset','feBlend','symbol','pattern'] // 不完整 完整太多 自己可以添加
                 if(svgs.indexOf(ele[i].type) !== -1) {
                     dom = document.createElementNS('http://www.w3.org/2000/svg',ele[i].type) ; // svg 图标的生成
                 }else {
                     dom = document.createElement(ele[i].type) ;
                 }
                 
                 for (var key in ele[i].props) {
                      if(key !== 'children') {
                          if (key === 'className') {
                            dom.setAttribute('class',ele[i].props[key]) ;
                          }else if (key === 'style') {
                            let styleTxt = ''
                            Object.keys(ele[i].props[key]).forEach(keys => {
                             styleTxt += keys + ":" + ele[i].props[key][keys] + ';'
                            })
                            dom.setAttribute('style',styleTxt)
                        }else {
                            dom.setAttribute(key,ele[i].props[key]) ;
                        }
                      }
                    //   console.log(key,ele[i].props[key])
                     
                 }
                 arr.push({
                     ele:dom,
                     val
                 })
             }
         }
          return arr ;

     } ;
     // 文字的延迟加载 
     const setWord = function(ele,word,callback) {
         setTimeout(() => {
             if(word == "💎") {
                ele.ele.innerHTML = word ;  
             }else {
                ele.ele.appendChild(document.createTextNode(word)) ;
             }
             callback(ele) ;
         }, 120);
     } ;
     // 判断是文字还是标签还是样式
     const takecartton = function(ele) {
          if(ele.val.length === 0) {
              if(ele.parent) return takecartton(ele.parent) ;
              return 
          }
          let currentValue = ele.val.shift() ;
          if (typeof currentValue == 'string') {
            setWord(ele,currentValue,takecartton) ;
          }else if(typeof currentValue == 'object') {
            //   currentValue.ele.se
              ele.ele.appendChild(currentValue.ele);
              currentValue.parent = ele ; // 设置父级对象 重复利用
              takecartton(currentValue)
          }

     } ;

     useEffect(()=>{
        const ars = getArr([children],[]) ;
        const origin = {
            parent:null,
            ele:refs.current,
            val:ars
        }
        const wos = origin 
        console.log(ars[0]) ;
        // setTimeout(()=>{
            takecartton(wos) ;
        // },1000)
        return ()=>{

        }
    },[])
    return (
        <div className='ccccc' ref={refs}>
           {/* {children} */}
        
        </div>
    )
}

export default Index
