import React, { useRef } from "react";
import { Carousel } from "antd";
import "./swiper.scss";

const imgs = ["/images/robet.jpg", "/images/883.jpg", "/images/820.jpg", "/images/835.jpg"];
function Index() {
  // const imgslist = [] ;
  const container = useRef();
  React.useEffect(() => {}, []);
  return (
    <div style={{ height: "100%", width: "100%" }} className="ccc" ref={container}>
      <Carousel autoplay>
        {imgs.map((item,index) => {
          return (
            <div key={index}>
              <img src={item} alt="" />
            </div>
          );
        })}
      </Carousel>
    </div>
  );
}

export default Index;
