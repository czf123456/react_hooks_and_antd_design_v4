import { createFromIconfontCN } from '@ant-design/icons';

const MyIcon = createFromIconfontCN({
  scriptUrl: '/js/aliyunstyle.js', // 在 iconfont.cn 上生成 注意 想要动态改变图标颜色 必须在iconfont上图标去色
});

export default MyIcon ;