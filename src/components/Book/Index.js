import React, { useEffect, useState,useMemo } from "react";
import "./book.scss";
function Index({ pages,author,name,bookcolor,backDes,cover,keys,dispatch,state }) {
  const [active, setActive] = useState(false);
  const [flip, setFlip] = useState(false);
  const [current, setCurrent] = useState(0);
  // const activeKey = useMemo(()=>{
  //      if(state.key !== keys) {
  //       setActive((prev) => false);
  //      }
  //     return state.key
  // },[state.key])
  useEffect(() => {});
  return (
    <div className="book" style={{'zIndex':keys === state.key? 10000:1}}>
      <div className="main">
        <div className="outer-book" style={{ width: "300px" }}>
          <div
            className={`bk-book book ${!flip && !active ? "bk-bookdefault" : ""} ${
              active && !flip&&state.key===keys ? "bk-viewinside" : ""
            } ${flip && !active ? "bk-viewback" : ""} `}
          >
            <div className="bk-front">
              <div className="bk-cover" style={{backgroundColor:bookcolor}}>
                  {cover}
               
              </div>
              <div className="bk-cover-back" style={{backgroundColor:bookcolor}}></div>
            </div>
            <div className="bk-page">
              {pages.map((item, index) => (
                <div
                  key={index}
                  className={`bk-content ${index == current ? "bk-content-current" : ""}`}
                >
                  <p>{item}</p>
                </div>
              ))}
              <span className='pagenum'>{current+1}</span>
              {pages.length > 1 ? (
                <nav>
                  <span
                    className="bk-page-prev"
                    onClick={() => {
                      console.log(current);
                      if (current >= 1) {
                        setCurrent((prev) => --prev);
                      }
                    }}
                  >
                    &lt;
                  </span>
                  <span
                    className="bk-page-next"
                    onClick={() => {
                      console.log(current);
                      if (current <= pages.length - 2) {
                        setCurrent((prev) => ++prev);
                      }
                    }}
                  >
                    &gt;
                  </span>
                </nav>
              ) : (
                <span className='nocontent'>暂无内容</span>
              )}
            </div>
            <div className="bk-back" style={{backgroundColor:bookcolor}}>
               {backDes}
            </div>
            <div className="bk-right"></div>
            <div className="bk-left" style={{backgroundColor:bookcolor}}>
              <h2>
                <span>{name}</span>
                <span>{author}</span>
              </h2>
            </div>
            <div className="bk-top"></div>
            <div className="bk-bottom"></div>
          </div>
          <div className="bk-info">
            <button
              className="bk-bookback"
              onClick={() => {
                setActive((prev) => false);
                setFlip((prev) => !prev);
              }}
            >
              反&emsp;&emsp;转
            </button>
            <button
              className={`bk-bookview ${active ? "bk-active" : ""}`}
              onClick={() => {
                dispatch(keys);
                setActive((prev) => !prev);
                setFlip((prev) => false);
              }}
            >
              {active ? '关闭书籍' : '打开书籍' }
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Index;
