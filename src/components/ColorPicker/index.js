import React, { useState, useCallback } from "react";
import {message} from 'antd'
import { SketchPicker } from "react-color";
function Index(props) {
  const [color, setColor] = useState("#13C2C2");
  //   const handlehandle = function(colorCode) {
  //       console.log(colorCode) ;
  //        setColor(colorCode.hex)
  //    };
  // const handleGoods = useCallback(
  //   (colorCode) => {
        
  //   //   document.querySelector(".ant-menu-item-selected").style.backgroundColor = color;
  //   },
  //   [color]
  // );
  const handlehandle = useCallback(
    (colorCode) => {
      console.log(colorCode.hex);
      setColor(colorCode.hex);
      window.less
      .modifyVars({
        "@primary-color":colorCode.hex,
      })
      .then(() => {
        localStorage.setItem('primaryColor',colorCode.hex)
        message.success(`修改主题成功`);
      })
      .catch((error) => {
        message.error(`Failed to update theme`);
      });
    },
    [color]
  );
  return (
    <div>
      <SketchPicker
      disableAlpha={true}
        onChangeComplete={handlehandle}
        color={color}
      />
    </div>
  );
}

export default Index;
