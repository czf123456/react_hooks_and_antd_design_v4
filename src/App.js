import React, { useState,useEffect } from "react";
import { Layout, Menu,Avatar } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import {connect} from 'react-redux'
const { Header, Sider, Content } = Layout;
function App({UpateUserInfo,history}) {
  const [collapsed, setCollpased] = useState(false);
  const toggle = (Up) => {
    setCollpased(!collapsed);
  };
  useEffect(()=>{
  if (UpateUserInfo.userInfo === null) {
    cons
       history.push('/login') ;
  }
  },[])
  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className='avator' style={{height:collapsed? '76px':'65px',transition:'all 0.3s',cursor:'pointer'}}>
         <img src="/images/logo.png" alt="" style={{width:'200px',transform:collapsed ? 'scale(1.25)':'scale(1)',transition:'all 0.3s',transformOrigin:'left top'}}/>
        </div>
        <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
          <Menu.Item key="1">
            <UserOutlined />
            <span>nav 1</span>
          </Menu.Item>
          <Menu.Item key="2">
            <VideoCameraOutlined />
            <span>nav 2</span>
          </Menu.Item>
          <Menu.Item key="3">
            <UploadOutlined />
            <span>nav 3</span>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Header className="site-layout-background" style={{ padding: 0 }}>
          <svg className="icon"  aria-hidden="true" style={{fontSize:'26px',cursor:'pointer',marginLeft:20+'px'}} onClick={toggle}>
            <use  xlinkHref="#icon-shousuocaidan"></use>
          </svg>
        </Header>
        <Content
          className="site-layout-background"
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          Content
        </Content>
      </Layout>
    </Layout>
  );
}
export default connect(state=> state)(App);
